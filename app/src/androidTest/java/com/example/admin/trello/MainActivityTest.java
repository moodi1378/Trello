package com.example.admin.trello;

import android.support.test.runner.AndroidJUnit4;

import com.example.admin.trello.views.activities.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.rule.ActivityTestRule;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mainAcRule
            = new ActivityTestRule<>(MainActivity.class);
}
