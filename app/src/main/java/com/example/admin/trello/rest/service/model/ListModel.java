package com.example.admin.trello.rest.service.model;

import com.google.gson.annotations.SerializedName;

public class ListModel {

    @SerializedName("id")
    private String listID;

    @SerializedName("name")
    private String listName;

    @SerializedName("closed")
    private boolean isClosed;

    public ListModel(String listID, String listName, boolean isClosed) {
        this.listID = listID;
        this.listName = listName;
        this.isClosed = isClosed;
    }

    public String getListID() {
        return listID;
    }

    public String getListName() {
        return listName;
    }

    public boolean isClosed() {
        return isClosed;
    }
}
