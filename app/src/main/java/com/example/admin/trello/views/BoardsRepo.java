package com.example.admin.trello.views;

import com.example.admin.trello.rest.service.model.Board;

import java.util.ArrayList;
import java.util.List;

public class BoardsRepo {

    private List<Board> AllBoards;
    private List<Board> personalBoards = new ArrayList<>();
    private List<Board> starredBoards = new ArrayList<>();
    private List<Board> closeBoards = new ArrayList<>();

    public List<Board> getAllBoards() {
        return AllBoards;
    }

    public List<Board> getPersonalBoards() {
        return personalBoards;
    }

    public List<Board> getStarredBoards() {
        return starredBoards;
    }

    public List<Board> getCloseBoards() {
        return closeBoards;
    }

    public void setAllBoards(List<Board> allBoards) {
        this.AllBoards = allBoards;
        clearBoards();
        separateBoards();
    }

    /**
     * find the starred and personal AllBoards and
     * separate them to
     * {@link #personalBoards} and
     * {@link #starredBoards}
     */
    private void separateBoards() {
        for (int i = 0; i < AllBoards.size(); i++) {
            Board board = AllBoards.get(i);

            if (board.isClosed()) {
                closeBoards.add(board);
            } else {
                if (board.isStarred()) {
                    starredBoards.add(board);
                } else {
                    personalBoards.add(board);
                }
            }
        }
    }

    private void clearBoards() {
        starredBoards.clear();
        personalBoards.clear();
        closeBoards.clear();
    }
}
