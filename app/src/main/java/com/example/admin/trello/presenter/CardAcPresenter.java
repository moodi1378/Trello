package com.example.admin.trello.presenter;

import com.example.admin.trello.rest.service.client.ApiBoardRestClient;
import com.example.admin.trello.rest.service.model.Card;
import com.example.admin.trello.util.Subscriber;
import com.example.admin.trello.views.activities.BaseActivity;

public class CardAcPresenter implements CardAcContract.Presenter{

    private ApiBoardRestClient client;
    private CardAcContract.View view;

    public CardAcPresenter(CardAcContract.View view) {
        this.view = view;
        client = ApiBoardRestClient.getInstance();
    }


    @Override
    public void getCard(String cardId) {
        new Subscriber<Card>(
                client.getCard(cardId, BaseActivity.TOKEN),
                new Subscriber.SubsCallBack<Card>() {
                    @Override
                    public void onResponse(Card response) {

                    }
                }
        );
    }
}
