package com.example.admin.trello.views;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.Log;

import com.example.admin.trello.rest.service.model.BoardLists;
import com.example.admin.trello.views.fragments.ListFragment;

import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = "ViewPagerAdapter";

    private List<BoardLists> boardLists;

    public ViewPagerAdapter(FragmentManager fm, List<BoardLists> boardLists) {
        super(fm);
        this.boardLists = boardLists;
        Log.d(TAG, "ViewPagerAdapter: " + boardLists.size());
    }

    @Override
    public int getCount() {
        return boardLists.size() + 1;
    }

    @Override
    public Fragment getItem(int position) {
        System.out.println(position);
        Log.d(TAG, "getItem: " + position);
        if (position == boardLists.size() || boardLists.size() == 0) {
            return ListFragment.newInstance(null, position);
        }
        return ListFragment.newInstance(boardLists.get(position), position);
    }

    // This is called when notifyDataSetChanged() is called
    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return PagerAdapter.POSITION_NONE;
    }

}