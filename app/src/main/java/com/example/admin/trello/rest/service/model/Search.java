package com.example.admin.trello.rest.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Search {

    @SerializedName("boards")
    List<Board> boards;

    @SerializedName("cards")
    List<Card> cards;

    public List<Board> getBoards() {
        return boards;
    }

    public List<Card> getCards() {
        return cards;
    }
}
