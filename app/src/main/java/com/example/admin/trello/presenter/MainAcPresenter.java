package com.example.admin.trello.presenter;


import android.util.Log;

import com.example.admin.trello.rest.service.client.ApiBoardRestClient;
import com.example.admin.trello.rest.service.model.Board;
import com.example.admin.trello.util.Subscriber;
import com.example.admin.trello.views.activities.BaseActivity;

import java.util.List;

import io.reactivex.disposables.Disposable;

public class MainAcPresenter implements MainAcContract.Presenter {
    private static final String TAG = "MainAcPresenter";

    private ApiBoardRestClient client;
    private MainAcContract.View view;
    private Disposable disposable;

    public MainAcPresenter(MainAcContract.View view) {
        this.view = view;
        client = ApiBoardRestClient.getInstance();
    }

    @Override
    public void getBoards() {
        new Subscriber<List<Board>>(
                client.getBoards(BaseActivity.TOKEN),
                new Subscriber.SubsCallBack<List<Board>>() {
                    @Override
                    public void onResponse(List<Board> response) {
                        Log.d(TAG, "onResponse: in the get");

                        if (response != null) {
                            view.onBoardFetched(response);
                        }
                    }
                }
        );
    }

    @Override
    public void addBoard(String boardName) {
        new Subscriber<Board>(client.addBoard(boardName, BaseActivity.TOKEN), new Subscriber.SubsCallBack<Board>() {
            @Override
            public void onResponse(Board response) {
                Log.d(TAG, "onResponse: in the add");
                view.onBoardAdded(response);
            }

        });
    }
}
