package com.example.admin.trello.views.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.admin.trello.R;
import com.example.admin.trello.rest.service.model.Board;
import com.example.admin.trello.views.event.onBoardClicked;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LinearBoardRecyAdp extends RecyclerView.Adapter<LinearBoardRecyAdp.ViewHolder> {
    private static final String TAG = "LinearBoardRecyAdp";

    private  Context context;
    private List<Board> mBoard;

    public LinearBoardRecyAdp(Context context, List<Board> boards) {
        this.context = context;
        this.mBoard = boards;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.recent_recy_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Board board = mBoard.get(i);
        viewHolder.boardName.setText(board.getBoardName());
        if (!board.isStarred()) {
            viewHolder.starImg.setVisibility(View.INVISIBLE);
        }
        String imgUrl;
        if (board.getBoardPreferences().getBackgroundImageScaled() != null) {
            imgUrl = board.getBoardPreferences().getBackgroundImageScaled().get(3).getUrl();
            Glide.with(context).load(imgUrl).into(viewHolder.boardImg);
        }

        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new onBoardClicked(board));
            }
        });
        viewHolder.layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                EventBus.getDefault().post(new OnLongClickBoard(board));
                return true;
            }
        });


    }

    @Override
    public int getItemCount() {
        if (mBoard != null) {
            return mBoard.size();
        } else {
            return 0;
        }
    }


    public void notifyBoards(List<Board> mBoard) {
        this.mBoard = mBoard;
        Log.d(TAG, "notifyBoards: board name is : " + this.mBoard.get(0).getBoardName());
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.normal_board_img)
        ImageView boardImg;
        @BindView(R.id.normal_board_star)
        ImageView starImg;
        @BindView(R.id.normal_board_name)
        TextView boardName;
//        @BindView(R.id.normal_board_layout)
//        ConstraintLayout boardLayout;

        View layout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            layout = itemView;
        }
    }
}
