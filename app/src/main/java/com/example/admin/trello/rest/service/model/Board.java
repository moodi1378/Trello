package com.example.admin.trello.rest.service.model;

import com.google.gson.annotations.SerializedName;

public class Board {

    @SerializedName("name")
    private String boardName;

    @SerializedName("closed")
    private boolean isClosed;

    @SerializedName("id")
    private String boardID;

    @SerializedName("starred")
    private boolean isStarred;

    @SerializedName("prefs")
    private Preference boardPreferences;

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    public String getBoardID() {
        return boardID;
    }

    public void setBoardID(String boardID) {
        this.boardID = boardID;
    }

    public boolean isStarred() {
        return isStarred;
    }

    public void setStarred(boolean starred) {
        isStarred = starred;
    }

    public Preference getBoardPreferences() {
        return boardPreferences;
    }

    public void setBoardPreferences(Preference boardPreferences) {
        this.boardPreferences = boardPreferences;
    }
}