package com.example.admin.trello.presenter;

import android.util.Log;

import com.example.admin.trello.rest.service.client.ApiBoardRestClient;
import com.example.admin.trello.rest.service.model.BoardLists;
import com.example.admin.trello.rest.service.model.ListCard;
import com.example.admin.trello.rest.service.model.ListModel;
import com.example.admin.trello.util.Subscriber;
import com.example.admin.trello.views.activities.BaseActivity;

import java.util.List;

import io.reactivex.Single;

public class ListAcPresenter implements ListAcContract.Presenter {
    private static final String TAG = "ListAcPresenter";

    private ListAcContract.View view;
    private ApiBoardRestClient client;

    public ListAcPresenter(ListAcContract.View view) {
        this.view = view;
        client = ApiBoardRestClient.getInstance();
    }


    @Override
    public void getList(String boardID) {
        Single<List<BoardLists>> lists = client.getListByBoardId(boardID, BaseActivity.TOKEN);

        new Subscriber<List<BoardLists>>(lists,
                new Subscriber.SubsCallBack<List<BoardLists>>() {
                    @Override
                    public void onResponse(List<BoardLists> response) {
                        Log.d(TAG, "onResponse: got the lists" + response);
                        view.onGettingListResponse(response);
                    }
                });
    }

    @Override
    public void addList(String listName, final String boardID) {
        new Subscriber<ListModel>(client.addList(listName, boardID, BaseActivity.TOKEN),
                new Subscriber.SubsCallBack<ListModel>() {
                    @Override
                    public void onResponse(ListModel response) {
                        view.onAddingListResponse(response);
                    }
                });
    }

    @Override
    public void addCard(String name, String desc, String listId) {
        new Subscriber<ListCard>(client.addCard(name, desc, listId, BaseActivity.TOKEN),
                new Subscriber.SubsCallBack<ListCard>() {
                    @Override
                    public void onResponse(ListCard response) {
                        if (response != null) {
                            view.onAddingCardResponse(response);
                        }
                    }
                });
    }
}
