package com.example.admin.trello.rest.service.model;

import com.google.gson.annotations.SerializedName;

public class BackgroundImgScaled {

    @SerializedName("width")
    private Integer bgWidth;

    @SerializedName("height")
    private Integer bgHeight;

    @SerializedName("url")
    private String  bgUrl;


    public Integer getWidth() {
        return bgWidth;
    }

    public Integer getHeight() {
        return bgHeight;
    }

    public String getUrl() {
        return bgUrl;
    }
}
