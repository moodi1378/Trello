package com.example.admin.trello.rest.service.api;

import com.example.admin.trello.rest.service.model.Board;
import com.example.admin.trello.rest.service.model.BoardLists;
import com.example.admin.trello.rest.service.model.Card;
import com.example.admin.trello.rest.service.model.ListCard;
import com.example.admin.trello.rest.service.model.ListModel;
import com.example.admin.trello.rest.service.model.Search;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiBoard {

    @GET("/1/members/me/boards")
    Single<List<Board>> getBoards(
            @Query("key") String key,
            @Query("token") String token
    );

    @GET("/1/search")
    Single<Search> search(
            @Query("query") String query,
            @Query("board_fields") String fields,
            @Query("card_board") String hasBoard,
            @Query("card_list") String hadList,
            @Query("partial") String ignoreCase,
            @Query("key") String key,
            @Query("token") String token
    );

    @GET("/1/boards/{boardId}/lists")
    Single<List<BoardLists>> getListByBoardId(
            @Path("boardId") String boradId,
            @Query("cards") String cards,
            @Query("card_fields") String card_fields,
            @Query("filter") String filter,
            @Query("fields") String fields,
            @Query("key") String key,
            @Query("token") String token
    );


    @GET("/1/cards/{cardId}")
    Single<Card> getCardById(
            @Path("cardId") String cardId,
            @Query("fields") String fields,
            @Query("board") String board,
            @Query("list") String list,
            @Query("key") String key,
            @Query("token") String token
    );


    @POST("/1/boards/")
    Single<Board> addBoard(
            @Query("name") String name,
            @Query("key") String key,
            @Query("token") String token
    );

    @POST("/1/lists")
    Single<ListModel> addList(
            @Query("name") String name,
            @Query("idBoard") String idBoard,
            @Query("pos") String pos,
            @Query("key") String key,
            @Query("token") String token
    );

    @POST("/1/cards")
    Single<ListCard> addCard(
            @Query("name") String name,
            @Query("desc") String desc,
            @Query("idList") String listId,
            @Query("keepFromSource") String source,
            @Query("key") String key,
            @Query("token") String token
    );

}
