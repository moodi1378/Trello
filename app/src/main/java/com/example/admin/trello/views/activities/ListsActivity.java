package com.example.admin.trello.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.admin.trello.R;
import com.example.admin.trello.presenter.ListAcContract;
import com.example.admin.trello.presenter.ListAcPresenter;
import com.example.admin.trello.rest.service.model.Board;
import com.example.admin.trello.rest.service.model.BoardLists;
import com.example.admin.trello.rest.service.model.Card;
import com.example.admin.trello.rest.service.model.ListCard;
import com.example.admin.trello.rest.service.model.ListModel;
import com.example.admin.trello.views.CustomViewPager;
import com.example.admin.trello.views.ViewPagerAdapter;
import com.example.admin.trello.views.event.AddNewList;
import com.example.admin.trello.views.event.FragmentPositinAware;
import com.example.admin.trello.views.event.OpenningCard;
import com.example.admin.trello.views.event.SendBaordToListActiv;
import com.example.admin.trello.views.event.SendCardToListActiv;
import com.example.admin.trello.views.fragments.AddCardDialogFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListsActivity extends BaseActivity implements ListAcContract.View {
    private static final String TAG = "ListsActivity";

    public ListAcContract.Presenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_tool_lay)
    public ConstraintLayout mainToolLay;
    @BindView(R.id.add_tool_lay)
    public ConstraintLayout addToolLay;
    @BindView(R.id.list_activity_view_pg)
    public CustomViewPager viewPager;
    @BindView(R.id.toolbar_board_name)
    TextView boardName;

    public Board currentBoard;
    List<BoardLists> lists;
    public Card searchedCard;
    public int currentListPos = 0;
    public int currentCardPos = 0;

    //    boolean listAdded = false;
    private int viewPagerSelectedItem;
    private boolean shouldOpenCardActiv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Log.d(TAG, "onCreate: ");

        ButterKnife.bind(this);
        presenter = new ListAcPresenter(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        viewPagerInit();
        showProgress("Please Wait ..");
    }

    private void viewPagerInit() {
        viewPager.setClipToPadding(false);
        viewPager.setPadding(120, 0, 120, 0);
        viewPager.setPageMargin(60);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                viewPagerSelectedItem = i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onBoardGet(SendBaordToListActiv event) {
        EventBus.getDefault().removeStickyEvent(event);
        currentBoard = event.board;
        Log.d(TAG, "onBoardGet: " + currentBoard.getBoardName());
        Log.d(TAG, "onBoardGet: thread = " + Thread.currentThread().getName());
        Log.i(TAG, "onBoardGet: comming from " + currentBoard.getBoardID());
        boardName.setText(event.board.getBoardName());
        getBoardList(currentBoard);
    }

    public void getBoardList(Board board) {

        presenter.getList(board.getBoardID());
        Log.d(TAG, "getBoardList: ");
    }

    @Override
    public void onGettingListResponse(List<BoardLists> lists) {
        hideProgress();
        this.lists = lists;
        showLists();
    }

    ViewPagerAdapter adaptor;

    private void showLists() {
        adaptor = new ViewPagerAdapter(getSupportFragmentManager(), lists);

        viewPager.setOffscreenPageLimit(lists.size() + 1);
        viewPager.setAdapter(adaptor);
        viewPager.setCurrentItem(currentCardPos);

        viewPager.setCurrentItem(currentListPos);
        currentListPos = 0;

        if (shouldOpenCardActiv) {
            viewPager.setCurrentItem(findTheCurrentlist());
            goToCardActivity(searchedCard.getCardId(), searchedCard.getCardName());
        }
    }

    private int findTheCurrentlist() {
        for (int i = 0; i < lists.size(); i++) {
            if (searchedCard.getListModelCard().getListID().equals(lists.get(i).getListID())) {
                return i;
            }
        }
        return 0;
    }

    @OnClick(R.id.toolbar_add_list_done)
    public void onDoneAddingList() {
        EventBus.getDefault().post(new AddNewList(true));
    }

    public void addNewList(String listName) {
        // showProgress("Please Wating...");
        hideingAddToolbar();
        presenter.addList(listName, currentBoard.getBoardID());
    }


    @Override
    public void onAddingListResponse(ListModel addedList) {
        lists.add(new BoardLists(addedList.getListID(), addedList.getListName(), addedList.isClosed()));
//        showLists();
        adaptor.notifyDataSetChanged();
        currentListPos = lists.size() - 1;
        // hideProgress();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(SendCardToListActiv event) {
        //EventBus.getDefault().removeStickyEvent(SendCardToListActiv.class);
        searchedCard = event.card;
        currentBoard = searchedCard.getBoardCard();
        shouldOpenCardActiv = true;
        getBoardList(event.card.getBoardCard());
    }

    public void goToCardActivity(String cardId, String cardName) {
        shouldOpenCardActiv = false;
        EventBus.getDefault().postSticky(new OpenningCard(cardId, cardName));
        Intent intent = new Intent(ListsActivity.this, CardActivity.class);
        startActivityForResult(intent, RESULT_OK);
    }

    @Subscribe
    public void onAddCardClick(AddCardDialogFragment.OnAddClicked event) {
        //showProgress("Please Wait..");
        Log.d(TAG, "onAddCardClick: adding card");
        presenter.addCard(event.name, event.desc, event.listId);
    }

    @Override
    public void onAddingCardResponse(ListCard card) {
        if (card != null) {
            currentCardPos = viewPager.getCurrentItem();
            lists.get(currentCardPos).addCard(card);
            adaptor.notifyDataSetChanged();
        }
        // hideProgress();
//        getBoardList(currentBoard);
    }

    @OnClick(R.id.toolbar_add_list_close)
    public void onCloseAddingListClick() {
        hideingAddToolbar();
        EventBus.getDefault().post(new AddNewList(false));
    }

    public void hideingAddToolbar() {
        viewPager.setPagingEnabled(true);
        mainToolLay.setVisibility(View.VISIBLE);
        addToolLay.setVisibility(View.GONE);
    }

    public void showAddToolbar() {
        viewPager.setPagingEnabled(false);
        mainToolLay.setVisibility(View.GONE);
        addToolLay.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onViewpagerItemPositionAware(FragmentPositinAware event) {
        if (event.pos != viewPagerSelectedItem) {
            viewPager.setCurrentItem(event.pos, true);
        }

        if (event.pos == lists.size() - 1) {
            onCloseAddingListClick();
        }
    }

    @OnClick(R.id.list_ac_back_btn)
    public void onBackpress() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

}
