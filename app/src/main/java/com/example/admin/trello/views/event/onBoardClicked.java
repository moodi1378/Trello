package com.example.admin.trello.views.event;


import com.example.admin.trello.rest.service.model.Board;

import java.io.Serializable;

public class onBoardClicked {
    public Board board;

    public onBoardClicked(Board board) {
        this.board = board;
    }
}
