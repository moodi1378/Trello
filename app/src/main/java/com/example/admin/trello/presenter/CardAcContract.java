package com.example.admin.trello.presenter;

import com.example.admin.trello.rest.service.model.Card;
import com.example.admin.trello.rest.service.model.Search;

public interface CardAcContract {

    public interface Presenter {
        void getCard(String cardId);
    }

    public interface View {
        void onGettingCardDetail(Card card);
    }
}
