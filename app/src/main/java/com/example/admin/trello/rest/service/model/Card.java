package com.example.admin.trello.rest.service.model;

import com.google.gson.annotations.SerializedName;

public class Card {

    @SerializedName("id")
    private String cardId;

    @SerializedName("name")
    private String cardName;

    @SerializedName("closed")
    private boolean isClosed;

    @SerializedName("desc")
    private String cardDesc;

    @SerializedName("board")
    private Board BoardCard;

    @SerializedName("list")
    private ListModel listModelCard;

    public String getCardId() {
        return cardId;
    }

    public String getCardName() {
        return cardName;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public String getCardDesc() {
        return cardDesc;
    }

    public Board getBoardCard() {
        return BoardCard;
    }

    public ListModel getListModelCard() {
        return listModelCard;
    }

    public Card(String cardId, String cardName, boolean isClosed, String cardDesc, Board boardCard, ListModel listModelCard) {
        this.cardId = cardId;
        this.cardName = cardName;
        this.isClosed = isClosed;
        this.cardDesc = cardDesc;
        BoardCard = boardCard;
        this.listModelCard = listModelCard;
    }
}
