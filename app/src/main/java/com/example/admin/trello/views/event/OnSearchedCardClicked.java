package com.example.admin.trello.views.event;

import com.example.admin.trello.rest.service.model.Card;

public class OnSearchedCardClicked {
    public Card card;

    public OnSearchedCardClicked(Card card) {
        this.card = card;
    }
}
