package com.example.admin.trello.rest.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BoardLists implements Parcelable {

    @SerializedName("id")
    private String listID;

    @SerializedName("name")
    private String listName;

    @SerializedName("closed")
    private boolean isClosed;

    @SerializedName("cards")
    private List<ListCard> cards;

    public BoardLists(String listID, String listName, boolean isClosed) {
        this.listID = listID;
        this.listName = listName;
        this.isClosed = isClosed;
        this.cards = new ArrayList<>();
    }

    protected BoardLists(Parcel in) {
        listID = in.readString();
        listName = in.readString();
        isClosed = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(listID);
        dest.writeString(listName);
        dest.writeByte((byte) (isClosed ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BoardLists> CREATOR = new Creator<BoardLists>() {
        @Override
        public BoardLists createFromParcel(Parcel in) {
            return new BoardLists(in);
        }

        @Override
        public BoardLists[] newArray(int size) {
            return new BoardLists[size];
        }
    };

    public String getListID() {
        return listID;
    }

    public String getListName() {
        return listName;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public List<ListCard> getCards() {
        return cards;
    }

    public void addCard(ListCard card) {
        cards.add(card);
    }

}
