package com.example.admin.trello.views.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.example.admin.trello.R;
import com.example.admin.trello.SimpleIdlingRes;
import com.example.admin.trello.library.StickyScrollView;
import com.example.admin.trello.presenter.MainAcContract;
import com.example.admin.trello.presenter.MainAcPresenter;
import com.example.admin.trello.rest.service.model.Board;
import com.example.admin.trello.views.BoardsRepo;
import com.example.admin.trello.views.RtlGridLayoutManager;
import com.example.admin.trello.views.event.SendBaordToListActiv;
import com.example.admin.trello.views.event.onBoardClicked;
import com.example.admin.trello.views.fragments.AddBoardDialogFragment;
import com.example.admin.trello.views.fragments.BoardOptionDialogFragment;
import com.example.admin.trello.views.recyclerview.GrideBoardRecyAdp;
import com.example.admin.trello.views.recyclerview.LinearBoardRecyAdp;
import com.example.admin.trello.views.recyclerview.OnLongClickBoard;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Type;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainAcContract.View {

    private static final String TAG = "MainActivity";

    // SharedPreferences name and key
    public static final String BOARDS = "boards";

    //View binding
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.sticky_scroll)
    StickyScrollView scrollView;
    @BindView(R.id.fab2)
    com.github.clans.fab.FloatingActionButton fab;
    @BindView(R.id.normal_recycler_view)
    RecyclerView recentRecyclerView;
    @BindView(R.id.starred_recycler_view)
    RecyclerView starredRecyclerView;
    @BindView(R.id.swipe_refresh_lay)
    SwipeRefreshLayout refreshLayout;

    LinearBoardRecyAdp linearBoardRecyAdp;
    GrideBoardRecyAdp grideBoardRecyAdp;

    BoardsRepo boardsRepo;

    MainAcContract.Presenter presenter;

    @Nullable
    private SimpleIdlingRes mIdlingResource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainAcPresenter(this);
        ButterKnife.bind(this);
        boardsRepo = new BoardsRepo();

        uiInit();
        getBoards();
    }

    private void uiInit() {
        initRecyclerView();
        fabInit();
        swipeRefreshLayout();
    }

    private void initRecyclerView() {
        recentRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        linearBoardRecyAdp = new LinearBoardRecyAdp(this, null);
        recentRecyclerView.setAdapter(linearBoardRecyAdp);

        starredRecyclerView.setLayoutManager(new RtlGridLayoutManager(this, 2));
        grideBoardRecyAdp = new GrideBoardRecyAdp(this, null);
        starredRecyclerView.setAdapter(grideBoardRecyAdp);
    }

    private void swipeRefreshLayout() {
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getBoards();
            }
        });
    }

    @Override
    protected void toolbarInit(Toolbar toolbar) {
        super.toolbarInit(toolbar);
    }

    private void fabInit() {
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > oldScrollY) {
                    fab.hide(true);
                }
                if (scrollY < oldScrollY) {
                    fab.show(true);
                }
            }
        });
    }

    @OnClick(R.id.main_toolbar_search)
    public void onSearchClicked() {
        Intent intent = new Intent(MainActivity.this, SearchActivity.class);
        startActivity(intent);
    }

    private void getBoards() {
        Log.d(TAG, "getAllBoards: ");
        //check if we had save the last fetched boards
        if (boardsRepo.getAllBoards() == null) {
            SharedPreferences prefs = getSharedPreferences(SH_PREFS_NAME, MODE_PRIVATE);
            String boards_str = prefs.getString(BOARDS, null);
            if (boards_str != null) {
                Gson gson = new Gson();
                Type type = new TypeToken<List<Board>>() {
                }.getType();
                List<Board> boards = gson.fromJson(boards_str, type);
                boardsRepo.setAllBoards(boards);
                showBoards();
            }
        }
        presenter.getBoards();
    }

    @Override
    public void onBoardFetched(List<Board> boards) {
        Log.d(TAG, "onBoardFetched: ");
        if (boards != null) {
            boardsRepo.setAllBoards(boards);
            showBoards();
            saveBoards();
        } else {
            Toast.makeText(this, "error on fetching boards", Toast.LENGTH_SHORT).show();
        }
        refreshLayout.setRefreshing(false);
    }

    // save all board in the system with sharedPref
    private void saveBoards() {
        SharedPreferences.Editor editor = getSharedPreferences(SH_PREFS_NAME, MODE_PRIVATE).edit();
        Gson gson = new Gson();
        String boards_str = gson.toJson(boardsRepo.getAllBoards());
        editor.putString(BOARDS, boards_str);
        editor.apply();
    }

    private void showBoards() {
        Log.d(TAG, "showBoards: called");
        linearBoardRecyAdp.notifyBoards(boardsRepo.getPersonalBoards());
        grideBoardRecyAdp.notifyBoards(boardsRepo.getStarredBoards());
    }

    // board click listener
    @Subscribe
    public void onBoardClicked(onBoardClicked board) {
        Log.i(TAG, "onBoardClicked: im main acitvity");
        Intent intent = new Intent(MainActivity.this, ListsActivity.class);
        EventBus.getDefault().postSticky(new SendBaordToListActiv(board.board));
        startActivity(intent);
    }

    @Subscribe
    public void onLongBoardClicked(OnLongClickBoard board) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("boardOption");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFragment = new BoardOptionDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BoardOptionDialogFragment.BOARD_NAME_KEY, board);
        dialogFragment.setArguments(bundle);
        dialogFragment.show(ft, "boardOption");
    }

    @Subscribe
    public void onStaringBoard(BoardOptionDialogFragment.OnStarBoardOpt board) {
//        ApiBoardRestClient.getInstance().starBoard(bo);
    }

    @OnClick(R.id.fab2)
    public void onFabClicked() {
        showAddDialog();
    }

    /**
     * adding board
     */
    private void showAddDialog() {
//        addBoard("added from app4");
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("add dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFragment = new AddBoardDialogFragment();
        dialogFragment.show(ft, "add dialog");
    }

    @Subscribe
    public void onDialogAddBtnClick(AddBoardDialogFragment.OnAddClicked newBoard) {
        showProgress("Adding...");
        presenter.addBoard(newBoard.name);
    }

    @Override
    public void onBoardAdded(Board board) {
        if (board != null) {
            onBoardClicked(new onBoardClicked(board));
        } else {
            Toast.makeText(this, "Error on adding board", Toast.LENGTH_SHORT).show();
        }
        hideProgress();
    }

    @VisibleForTesting
    @NonNull
    public IdlingResource getIdlingResource() {
        if (mIdlingResource == null) {
            mIdlingResource = new SimpleIdlingRes();
        }
        return mIdlingResource;
    }
}