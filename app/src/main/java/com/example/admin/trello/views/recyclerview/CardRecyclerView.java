package com.example.admin.trello.views.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.trello.R;
import com.example.admin.trello.rest.service.model.ListCard;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardRecyclerView extends RecyclerView.Adapter<CardRecyclerView.ViewHolder> {
    private static final String TAG = "CardRecyclerView";

    private Context context;
    private List<ListCard> cards;
    private OnCardClick listenenr;

    public CardRecyclerView(Context context, List<ListCard> cards, OnCardClick listenenr) {
        this.context = context;
        this.cards = cards;
        this.listenenr = listenenr;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_card_recy_item, viewGroup, false);
        return new CardRecyclerView.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final ListCard card = cards.get(i);
        viewHolder.cardName.setText(card.getCardName());
        System.out.println("descrip =   " + card.getCardDescription());
        if (!card.getCardDescription().equals("")) {
            viewHolder.desc.setVisibility(View.VISIBLE);
        }
        viewHolder.cardName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listenenr.onCardClicked(card.getCardID(), card.getCardName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cardName_txt)
        TextView cardName;
        @BindView(R.id.card_desc_imgv)
        ImageView desc;

        public View layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            layout = itemView;
        }
    }

    public interface OnCardClick {
        public void onCardClicked(String cardId, String cardName);
    }

}
