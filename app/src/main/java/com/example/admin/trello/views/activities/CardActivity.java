package com.example.admin.trello.views.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.example.admin.trello.R;
import com.example.admin.trello.presenter.CardAcContract;
import com.example.admin.trello.presenter.CardAcPresenter;
import com.example.admin.trello.rest.service.model.Card;
import com.example.admin.trello.views.event.OpenningCard;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardActivity extends BaseActivity implements CardAcContract.View{
    private static final String TAG = "CardActivity";

    private CardAcContract.Presenter presenter;

    //Binding Views
    @BindView(R.id.toolbar_card_name)
    TextView cardName;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    Card card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_acrtivity);
        ButterKnife.bind(this);
        presenter = new CardAcPresenter(this);
        showProgress("Please Wait..");
        toolbarInit(toolbar);

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onCardReceived(OpenningCard card) {
        EventBus.getDefault().removeStickyEvent(card);
        cardName.setText(card.cardName);
        hideProgress();
//        getCardDetail(card);
    }

    private void getCardDetail(OpenningCard card) {
        presenter.getCard(card.cardId);
    }

    @Override
    public void onGettingCardDetail(Card card) {
        this.card = card;
        showCardDetails();
    }

    private void showCardDetails() {
        Log.d(TAG, "showCardDetails: " + card.getBoardCard().getBoardID());
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().removeAllStickyEvents();
    }
}
