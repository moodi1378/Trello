package com.example.admin.trello.views.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.admin.trello.R;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddCardDialogFragment extends DialogFragment {


    @BindView(R.id.add_card_dia_name)
    EditText nameEdt;
    @BindView(R.id.add_card_dia_desc)
    EditText descEdt;
    @BindView(R.id.add_card_dia_btn)
    Button addBtn;

    String listId;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listId = getArguments().getString(ListFragment.LIST_ID_KEY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_card_dialog_layout, container, false);
        ButterKnife.bind(this, view);

        nameEdt.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);


        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = nameEdt.getText().toString();

                if (name.trim().matches("")) {
                    Toast.makeText(
                            getActivity().getApplicationContext(),
                            "Please Enter a Name",
                            Toast.LENGTH_LONG)
                            .show();

                    nameEdt.setText("");
                } else {
                    EventBus.getDefault().post(
                            new AddCardDialogFragment.OnAddClicked(name, descEdt.getText().toString(), listId)
                    );
                    dismiss();
                }
            }
        });

        return view;
    }

    public class OnAddClicked {
        public String name;
        public String desc;
        public String listId;

        public OnAddClicked(String name, String desc, String listId) {
            this.name = name;
            this.desc = desc;
            this.listId = listId;
        }
    }
}