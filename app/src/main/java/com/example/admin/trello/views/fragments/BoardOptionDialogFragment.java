package com.example.admin.trello.views.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.trello.R;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BoardOptionDialogFragment extends DialogFragment {

    public static final String BOARD_NAME_KEY = "boardname";

    @BindView(R.id.opt_dialog_board_name)
    TextView boardName;
    @BindView(R.id.opt_dialog_star_board)
    TextView starBoard;

    String name;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(BOARD_NAME_KEY, "");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.boardoption_dialog_layout, container, false);
        ButterKnife.bind(this, view);

        boardName.setText(name);

        starBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new OnStarBoardOpt());
            }
        });

        return view;
    }

    public class OnStarBoardOpt {
    }
}