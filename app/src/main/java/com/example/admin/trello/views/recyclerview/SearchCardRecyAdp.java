package com.example.admin.trello.views.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.trello.R;
import com.example.admin.trello.rest.service.model.Board;
import com.example.admin.trello.rest.service.model.Card;
import com.example.admin.trello.views.event.OnSearchedCardClicked;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchCardRecyAdp extends RecyclerView.Adapter<SearchCardRecyAdp.ViewHolder> {

    private Context context;
    private List<Card> mCard;

    public SearchCardRecyAdp(Context context, List<Card> cards) {
        this.context = context;
        this.mCard = cards;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.search_card_recy_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Card card = mCard.get(i);
        viewHolder.cardName.setText(card.getCardName());
        viewHolder.boardName.setText(card.getBoardCard().getBoardName());
        viewHolder.ListName.setText(card.getListModelCard().getListName());
        if (!card.getCardDesc().equals("")) {
            viewHolder.desc.setVisibility(View.VISIBLE);
        }

        // FIXME: 2/4/2019 fix the passed object in event bus
        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new OnSearchedCardClicked(card));
            }
        });
    }

    public void notifyCard(List<Card> mCard) {
        this.mCard = mCard;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mCard != null) {
            return mCard.size();
        } else {
            return 0;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.search_card_name)
        TextView cardName;
        @BindView(R.id.search_card_board_name)
        TextView boardName;
        @BindView(R.id.search_card_list_name)
        TextView ListName;
        @BindView(R.id.search_card_desc_imgv)
        ImageView desc;
        @BindView(R.id.search_card_view_name)
        CardView layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
