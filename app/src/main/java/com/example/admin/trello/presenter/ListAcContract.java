package com.example.admin.trello.presenter;

import com.example.admin.trello.rest.service.model.BoardLists;
import com.example.admin.trello.rest.service.model.ListCard;
import com.example.admin.trello.rest.service.model.ListModel;

import java.util.List;

public interface ListAcContract {

    public interface Presenter {
        void getList(String boardID);

        void addList(String listName, String boardID);

        void addCard(String name, String desc, String listId);
    }

    public interface View {
        void onGettingListResponse(List<BoardLists> lists);

        void onAddingListResponse(ListModel addedList);

        void onAddingCardResponse(ListCard card);
    }
}
