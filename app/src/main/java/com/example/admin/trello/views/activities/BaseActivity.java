package com.example.admin.trello.views.activities;

import android.app.ProgressDialog;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";
    public static final String TOKEN = "e0d4e544d82a77efdf2c8bfe15a0faba168bb53e1b8c27fb4fc1fb625f96e3a0";

    public final static String ACTIVITY_BOARD_KEY = "board";
    public final static String ACTIVITY_CARD_KEY = "card";
    public final static String ACTIVITY_List_KEY = "list";

    ProgressDialog dialog;

    //shared prefrences name
    public static final String SH_PREFS_NAME = "boards_shp";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    protected void toolbarInit(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    protected void onResume() {
        EventBus.getDefault().register(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        Log.d(TAG, "onPause: ");
        super.onPause();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    public void showProgress(String message) {
        dialog = new ProgressDialog(this);
        dialog.setMessage(message);
        dialog.setCancelable(false);
        dialog.show();
    }

    public void hideProgress() {
        dialog.dismiss();
    }
}
