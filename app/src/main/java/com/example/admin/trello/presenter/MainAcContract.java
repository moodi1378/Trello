package com.example.admin.trello.presenter;

import com.example.admin.trello.rest.service.model.Board;

import java.util.List;

public interface MainAcContract {

    public interface Presenter {
        void getBoards();

        void addBoard(String boardName);
    }

    public interface View {
        void onBoardFetched(List<Board> boards);
        void onBoardAdded(Board board);
    }

}
