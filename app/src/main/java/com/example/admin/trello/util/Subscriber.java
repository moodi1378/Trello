package com.example.admin.trello.util;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Subscriber<T> implements SingleObserver {
    private static final String TAG = "Subscriber";

    private Disposable disposable;
    private Single<T> singleT;
    private SubsCallBack<T> callback;

    public Subscriber(Single<T> singleT, SubsCallBack<T> callback) {
        this.singleT = singleT;
        this.callback = callback;
        subscribe();
    }

    public void subscribe() {

        singleT.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this);

    }

    @Override
    public void onSubscribe(Disposable d) {
        disposable = d;
    }

    @Override
    public void onSuccess(Object o) {
        callback.onResponse((T)o);
        disposable.dispose();
    }

    @Override
    public void onError(Throwable e) {
        callback.onResponse(null);
        disposable.dispose();
    }

    public interface SubsCallBack<T>{
        void onResponse(T response);
    }

}
