package com.example.admin.trello.views.recyclerview;


import com.example.admin.trello.rest.service.model.Board;

import java.io.Serializable;

public class OnLongClickBoard implements Serializable {

    public Board board;

    public OnLongClickBoard(Board board) {
        this.board = board;
    }
}
