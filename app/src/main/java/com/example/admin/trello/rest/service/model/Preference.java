package com.example.admin.trello.rest.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Preference {

    @SerializedName("permissionLevel")
    private String permissionLevel;

    @SerializedName("background")
    private String background;

    @SerializedName("backgroundImageScaled")
    private List<BackgroundImgScaled> backgroundImageScaled;

    public String getPermissionLevel() {
        return permissionLevel;
    }

    public String getBackground() {
        return background;
    }

    public List<BackgroundImgScaled> getBackgroundImageScaled() {
        return backgroundImageScaled;
    }
}
