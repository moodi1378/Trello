package com.example.admin.trello.views.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.trello.R;
import com.example.admin.trello.rest.service.model.BoardLists;
import com.example.admin.trello.views.activities.ListsActivity;
import com.example.admin.trello.views.event.AddNewList;
import com.example.admin.trello.views.event.FragmentPositinAware;
import com.example.admin.trello.views.recyclerview.CardRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment implements CardRecyclerView.OnCardClick {
    private static final String TAG = "ListFragment";

    private static final String BOARD_LIST = "boardList";
    private static final String POSITION = "position";

    public ListFragment() {
        // Required empty public constructor
    }

    public static final String LIST_ID_KEY = "listid";

    public static ListFragment newInstance(BoardLists param1, int position) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        if (param1 != null) {
            args.putParcelable(BOARD_LIST, param1);
        }
        args.putInt(POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.list_card_lay)
    ConstraintLayout cardLay;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.frag_text)
    TextView listNameTxt;
    @BindView(R.id.list_recycler_view)
    RecyclerView listRecyclerView;


    @BindView(R.id.list_add_lay)
    ConstraintLayout addLay;
    @BindView(R.id.add_list_edit_text)
    EditText newListName;
    @BindView(R.id.add_list_btn)
    Button addListBtn;
    @BindView(R.id.add_list_btn_frame)
    FrameLayout frameLayout;

    BoardLists list;
    ListsActivity activity;

    private int position;

    private boolean isThisAddListFragment = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().getParcelable(BOARD_LIST) != null) {
            list = getArguments().getParcelable(BOARD_LIST);
        } else {
            isThisAddListFragment = true;
        }
        position = getArguments().getInt(POSITION);
        activity = ((ListsActivity) getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);
        if (isThisAddListFragment) {
            cardLay.setVisibility(View.GONE);
            addLay.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager) activity.getBaseContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(newListName, InputMethodManager.SHOW_IMPLICIT);
        } else {
            listNameTxt.setText(list.getListName());
            showListCard();
        }

        return view;
    }

    // Card Lay Stuff
    private void showListCard() {
        listRecyclerView.setLayoutManager(new LinearLayoutManager(activity.getApplicationContext()));
        CardRecyclerView cardAdp = new CardRecyclerView(activity.getApplicationContext(), list.getCards(), this);
        listRecyclerView.setAdapter(cardAdp);
    }

    @Override
    public void onCardClicked(String cardId, String cardName) {
        sendPosition();
        activity.goToCardActivity(cardId, cardName);
    }

    @OnClick(R.id.add_card_btn)
    public void onAddCardClicked() {
        sendPosition();
        showDialogFragment();
    }

    private void showDialogFragment() {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        Fragment prev = activity.getSupportFragmentManager().findFragmentByTag("add list");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        AddCardDialogFragment fragment = new AddCardDialogFragment();
        Bundle b = new Bundle();
        b.putString(LIST_ID_KEY, list.getListID());
        fragment.setArguments(b);
        DialogFragment dialogFragment = fragment;
        dialogFragment.show(ft, "add list");
    }


    // add Lay Stuff
    @OnClick(R.id.add_list_btn)
    public void onAddingList() {
        sendPosition();
        activity.showAddToolbar();
        newListName.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.GONE);
    }

    @Subscribe
    public void Event(AddNewList event) {
        if (isThisAddListFragment) {
            if (event.Done) {
                String listName = newListName.getText().toString();
                if (!listName.trim().equals("")) {
                    oncloseAddingList();
                    activity.addNewList(listName);
                } else {
                    Toast.makeText(activity.getApplicationContext(), "Please enter a Name", Toast.LENGTH_LONG).show();
                }

            } else {
                oncloseAddingList();
            }
        } else {
            oncloseAddingList();
        }
    }

    private void oncloseAddingList() {
        newListName.setText("");
        newListName.setVisibility(View.GONE);
        frameLayout.setVisibility(View.VISIBLE);
    }

    private void sendPosition() {
        Log.d(TAG, "sendPosition: " + isThisAddListFragment);
        if (!isThisAddListFragment) {
            activity.hideingAddToolbar();
        }
        EventBus.getDefault().post(new FragmentPositinAware(position));
    }

    @Override
    public void onResume() {
        EventBus.getDefault().register(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }
}
