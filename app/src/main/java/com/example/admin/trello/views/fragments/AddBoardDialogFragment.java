package com.example.admin.trello.views.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.admin.trello.R;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddBoardDialogFragment extends DialogFragment {
    private static final String TAG = "AddBoardDialogFragment";

    @BindView(R.id.add_dialog_editt)
    EditText addEditText;
    @BindView(R.id.add_dialog_addbtn)
    Button addBtn;
    @BindView(R.id.add_dialog_disbtn)
    Button dismisBtn;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_board_dialog_layout, container, false);
        ButterKnife.bind(this, view);

        addEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = addEditText.getText().toString();

                if (name.trim().matches("")) {
                    Toast.makeText(
                            getActivity().getApplicationContext(),
                            "Please Enter a Name",
                            Toast.LENGTH_LONG)
                            .show();

                    addEditText.setText("");
                } else {
                    EventBus.getDefault().post(new OnAddClicked(name));
                    dismiss();
                }
            }
        });

        dismisBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }

    public void onClick(View view) {
        Log.d(TAG, "onColorClick: " + view.getTag());
    }

    public class OnAddClicked {
        public String name;

        public OnAddClicked(String name) {
            this.name = name;
        }
    }
}