package com.example.admin.trello.rest.service.client;

import com.example.admin.trello.rest.service.api.ApiBoard;
import com.example.admin.trello.rest.service.model.Board;
import com.example.admin.trello.rest.service.model.BoardLists;
import com.example.admin.trello.rest.service.model.Card;
import com.example.admin.trello.rest.service.model.ListCard;
import com.example.admin.trello.rest.service.model.ListModel;
import com.example.admin.trello.rest.service.model.Search;

import java.util.List;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiBoardRestClient {
    private static final String TAG = "ApiBoardRestClient";

    final String API_BASE_URL = "https://api.trello.com/";
    final String API_KEY = "0dfd592382f7eba0a032b56a1f280433";

    // Default queries
    String ALL = "all";
    String OPEN = "open";
    String ListPos = "bottom";
    String TRUE = "true";


    private static ApiBoardRestClient restClient;
    ApiBoard boardsService;


    public static ApiBoardRestClient getInstance() {
        if (restClient == null) {
            restClient = new ApiBoardRestClient();
        }
        return restClient;
    }

    public ApiBoardRestClient() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient httpClient =
                new OkHttpClient.Builder()
                        .addInterceptor(logging)
                        /* .addInterceptor(new Interceptor() {
                             @Override
                             public Response intercept(Chain chain) throws IOException {
                                 Request original = chain.request();
                                 HttpUrl originalHttpUrl = original.url();

                                 HttpUrl url = originalHttpUrl.newBuilder()
                                         .addQueryParameter("apikey", API_KEY)
                                         .build();

                                 // Request customization: add request headers
                                 Request.Builder requestBuilder = original.newBuilder()
                                         .url(url);

                                 Request request = requestBuilder.build();
                                 return chain.proceed(request);
                             }
                         })*/
                        .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .client(httpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        boardsService = retrofit.create(ApiBoard.class);
    }

    public Single<List<Board>> getBoards(String token) {
        return boardsService.getBoards(API_KEY, token);
    }

    public Single<Board> addBoard(String boardName, String token) {
        return boardsService.addBoard(boardName, API_KEY, token);
    }

    public Single<Search> search(String query, String token) {
        String boardFields = "closed,name,starred,prefs";
        return boardsService.search(query, boardFields, TRUE, TRUE, TRUE, API_KEY, token);
    }

    public Single<List<BoardLists>> getListByBoardId(String boardID, String token) {
        return boardsService.getListByBoardId(boardID, OPEN, ALL, OPEN, ALL, API_KEY, token);
    }

    public Single<ListModel> addList(String listName, String boardID, String token) {
        return boardsService.addList(listName, boardID, ListPos, API_KEY, token);
    }

    public Single<ListCard> addCard(String name, String desc, String listId, String token) {
        return boardsService.addCard(name, desc, listId, "all", API_KEY, token);
    }

    public Single<Card> getCard(String cardId, String token) {
        return boardsService.getCardById(cardId, ALL, TRUE, TRUE, API_KEY, token);
    }

}