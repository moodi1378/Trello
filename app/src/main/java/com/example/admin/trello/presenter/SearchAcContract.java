package com.example.admin.trello.presenter;

import com.example.admin.trello.rest.service.model.Board;
import com.example.admin.trello.rest.service.model.Search;

import java.util.List;

public interface SearchAcContract {

    public interface Presenter {
        void search(String query);
    }

    public interface View {
        void onSearchQueryResponse(Search searchResp);
    }
}
