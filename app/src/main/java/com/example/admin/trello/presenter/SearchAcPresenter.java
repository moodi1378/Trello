package com.example.admin.trello.presenter;

import com.example.admin.trello.rest.service.client.ApiBoardRestClient;
import com.example.admin.trello.rest.service.model.Search;
import com.example.admin.trello.util.Subscriber;
import com.example.admin.trello.views.activities.BaseActivity;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SearchAcPresenter implements SearchAcContract.Presenter {

    private ApiBoardRestClient client;
    private SearchAcContract.View view;

    public SearchAcPresenter(SearchAcContract.View view) {
        this.view = view;
        client = ApiBoardRestClient.getInstance();
    }

    @Override
    public void search(String query) {
        new Subscriber<Search>(
                client.search(query, BaseActivity.TOKEN),
                new Subscriber.SubsCallBack<Search>() {
                    @Override
                    public void onResponse(Search response) {
                        view.onSearchQueryResponse(response);
                    }
                }
        );
    }
}
