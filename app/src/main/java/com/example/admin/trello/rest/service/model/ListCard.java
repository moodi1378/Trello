package com.example.admin.trello.rest.service.model;

import com.google.gson.annotations.SerializedName;

public class ListCard {

    @SerializedName("id")
    private String cardID;

    @SerializedName("name")
    private String cardName;

    @SerializedName("closed")
    private boolean isClosed;

    @SerializedName("desc")
    private String cardDescription;


    public String getCardID() {
        return cardID;
    }

    public String getCardName() {
        return cardName;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public String getCardDescription() {
        return cardDescription;
    }
}
