package com.example.admin.trello.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.admin.trello.R;
import com.example.admin.trello.presenter.SearchAcContract;
import com.example.admin.trello.presenter.SearchAcPresenter;
import com.example.admin.trello.rest.service.model.Board;
import com.example.admin.trello.rest.service.model.Card;
import com.example.admin.trello.rest.service.model.Search;
import com.example.admin.trello.views.event.OnSearchedCardClicked;
import com.example.admin.trello.views.event.SendBaordToListActiv;
import com.example.admin.trello.views.event.SendCardToListActiv;
import com.example.admin.trello.views.recyclerview.LinearBoardRecyAdp;
import com.example.admin.trello.views.recyclerview.SearchCardRecyAdp;
import com.example.admin.trello.views.event.onBoardClicked;
import com.jakewharton.rxbinding3.widget.RxTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.subjects.PublishSubject;

public class SearchActivity extends BaseActivity implements SearchAcContract.View {
    private static final String TAG = "SearchActivity";

    private SearchAcContract.Presenter presenter;

    //Butterknife View Binding
    @BindView(R.id.search_toolbar)
    Toolbar toolbar;
    @BindView(R.id.search_edit_text)
    EditText searchEdittxt;
    @BindView(R.id.clear_search_text_view_btn)
    ImageButton clearTextBtn;
    @BindView(R.id.board_title_txt)
    TextView board_title_txt;
    @BindView(R.id.card_title_txt)
    TextView card_title_txt;
    @BindView(R.id.board_search_recycler_view)
    RecyclerView boardRecyclerView;
    @BindView(R.id.card_search_recycler_view)
    RecyclerView cardRecylerView;
    @BindView(R.id.search_swipe_refresh_layout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.nothin_found)
    TextView nothingFound;

    List<Card> cards;
    List<Board> boards;

    SearchCardRecyAdp cardRecyAdp;
    LinearBoardRecyAdp boardRecyAdp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        presenter = new SearchAcPresenter(this);
        refreshLayout.setEnabled(false);
        toolbarInit();
        initRecyclerView();
    }

    private void toolbarInit() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        RxTextView.textChanges(searchEdittxt)
                .map(new Function<CharSequence, String>() {
                    @Override
                    public String apply(CharSequence charSequence) throws Exception {
                        return charSequence.toString();
                    }
                })
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(String s) throws Exception {
                        if (!TextUtils.isEmpty(s.trim())) {
                            clearTextBtn.setVisibility(View.VISIBLE);
                            return true;
                        } else {
                            clearTextBtn.setVisibility(View.INVISIBLE);
                            nothingFound.setVisibility(View.INVISIBLE);
                            setBoardsVisibility(View.GONE);
                            setCardsVisibility(View.GONE);
                            return false;
                        }
                    }
                })
                .debounce(400, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String searchPar) {
                        refreshLayout.setRefreshing(true);
                        presenter.search(searchPar);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private void initRecyclerView() {
        boardRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        boardRecyAdp = new LinearBoardRecyAdp(this, null);
        boardRecyclerView.setAdapter(boardRecyAdp);

        cardRecylerView.setLayoutManager(new LinearLayoutManager(this));
        cardRecyAdp = new SearchCardRecyAdp(this, null);
        cardRecylerView.setAdapter(cardRecyAdp);
    }

    @OnClick(R.id.clear_search_text_view_btn)
    public void onSearchClear() {
        searchEdittxt.setText("");
        clearTextBtn.setVisibility(View.INVISIBLE);
        setBoardsVisibility(View.GONE);
        setCardsVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onSearchQueryResponse(Search searchResp) {
        boards = searchResp.getBoards();
        cards = searchResp.getCards();

        if (!searchEdittxt.getText().toString().trim().equals("")) {

            //first we invisible all the views then if they had something to show,
            // they can visible themselves
            setCardsVisibility(View.GONE);
            setBoardsVisibility(View.GONE);

            if (boards.size() == 0 && cards.size() == 0) {
                nothingFound.setVisibility(View.VISIBLE);
            } else {
                nothingFound.setVisibility(View.INVISIBLE);

                if (boards.size() != 0) {
                    setBoardsVisibility(View.VISIBLE);
                    showBoards();

                }
                if (cards.size() != 0) {
                    setCardsVisibility(View.VISIBLE);
                    showCards();
                }
            }
        }

        refreshLayout.setRefreshing(false);
    }

    private void showCards() {
        cardRecyAdp.notifyCard(cards);
    }

    private void showBoards() {
        boardRecyAdp.notifyBoards(boards);
    }

    @Subscribe
    public void onBoardClicked(onBoardClicked board) {
        Log.i(TAG, "onBoardClicked: in search view");
        EventBus.getDefault().postSticky(new SendBaordToListActiv(board.board));
        Intent intent = new Intent(SearchActivity.this, ListsActivity.class);
        startActivity(intent);
        finish();
    }

    @Subscribe
    public void onCardClicked(OnSearchedCardClicked card) {
        Log.d(TAG, "onCardClicked: in search view");
        Intent intent = new Intent(SearchActivity.this, ListsActivity.class);
        EventBus.getDefault().postSticky(new SendCardToListActiv(card.card));
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.serarch_back_btn)
    public void onBackClicked() {
        finish();
    }

    private void setBoardsVisibility(int visibility) {
        boardRecyclerView.setVisibility(visibility);
        board_title_txt.setVisibility(visibility);
    }

    private void setCardsVisibility(int visibility) {
        cardRecylerView.setVisibility(visibility);
        card_title_txt.setVisibility(visibility);
    }
}